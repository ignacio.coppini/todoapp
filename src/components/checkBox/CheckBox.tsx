import { IonItem, IonLabel, IonCheckbox } from "@ionic/react";
import { FunctionComponent, useEffect, useRef, useState } from "react";
import {Tarea} from '../../pages/Home'

interface CheckboxProps {
    Tarea: Tarea,
    user: string
    setListItems: any
    listItems: Tarea[]
}
 
const Checkbox: FunctionComponent<CheckboxProps> = ({Tarea, user, listItems ,setListItems}) => {
    const [checked, setCheked] = useState(Tarea.completed)
    const isFirstRender = useRef(true);

    useEffect(() => {
        if(!isFirstRender.current){
            CompleteItem();
        } else {
            isFirstRender.current = false;
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [checked])

    const CompleteItem = async () => {
        const response = await fetch(
          `https://api-3sxs63jhua-uc.a.run.app/v1/todo/${user}`,
          {
            method: "PUT",
            headers: { "content-type": "application/json" },
            body: JSON.stringify({
                completed: checked,
                todoId: Tarea.todoId ?? Tarea.id
            }),
          }
        );
        const data = await response.json();
        const updateList = listItems.map((tarea) => ({
            ...tarea, completed: tarea.todoId === Tarea.todoId ? checked : tarea.completed
        }))
        setListItems(updateList)
    };

    const removeItem = async () => {
        const { todoId } = Tarea;
        const response = await fetch(
          `https://api-3sxs63jhua-uc.a.run.app/v1/todo/${user}`,
          {
            method: "DELETE",
            headers: { "content-type": "application/json" },
            body: JSON.stringify({
                todoId
            }),
          }
        );
        const data = await response.json();
        const updateList = listItems.filter((tarea) => (
            tarea.todoId !== todoId
        ))
        setListItems(updateList)
    };

    

    return ( 
        <IonItem mode="ios" lines="none">
            <IonLabel>{Tarea.message}</IonLabel>
            <IonCheckbox mode='ios' slot="start" color="primary" checked={checked} onClick={() => setCheked(!checked)}/> 
        </IonItem>
    );
}
 
export default Checkbox;