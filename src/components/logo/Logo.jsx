import './logo.css'
import LOGO from './pcnt-logo.png'


const Logo = () => {
    return (
        <h1 className="logo">
            <img src={LOGO} alt='logo'/>
        </h1>

)}

export default Logo