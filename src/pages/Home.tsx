import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonContent,
  IonInput,
  IonList,
  IonLoading,
  IonPage,
  IonSelect,
  IonSelectOption,
  IonToolbar,
} from "@ionic/react";
import { useState, useEffect } from "react";
import Checkbox from "../components/checkBox/CheckBox";
import "./Home.css";
import Logo from '../components/logo/Logo'

export interface Tarea {
  title: string;
  completed: boolean;
  message: string;
  todoId?: string;
  id?: string;
}

const Home: React.FC = () => {
  const [item, setItem] = useState<string>("");
  const [listItems, setListItems] = useState<Tarea[]>([]);
  const [user, setUser] = useState<string>();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    (async () => {
      const response = await fetch(
        "https://api-3sxs63jhua-uc.a.run.app/v1/userId"
      );
      const data = await response.text();
      setUser(data);
    })();
  }, []);

  const AddItem = async () => {
    setIsLoading(true)
    const response = await fetch(
      `https://api-3sxs63jhua-uc.a.run.app/v1/todo/${user}`,
      {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({
          title: "Tareasdasda",
          message: item,
        }),
      }
    );
    const data = await response.json();
    setListItems([...listItems, data]);
    setItem("");
    setIsLoading(false)
  };

  const ResetAll = async () => {
    const response = await fetch(
      `https://api-3sxs63jhua-uc.a.run.app/v1/todo/${user}/reset`,
      {
        method: "DELETE",
        headers: { "content-type": "application/json" },
      }
    );
    const data = await response.json();
    setListItems([]);
  };

  const GetCompleted = async () => {
    setListItems([]);
    const response = await fetch(
      `https://api-3sxs63jhua-uc.a.run.app/v1/todo/${user}/true`,
      {
        method: "GET",
        headers: { "content-type": "application/json" },
      }
    );
    const data = await response.json();
    setListItems(data);
    setIsLoading(false);
  };

  const GetIncompleted = async () => {
    setListItems([]);
    const response = await fetch(
      `https://api-3sxs63jhua-uc.a.run.app/v1/todo/${user}/false`,
      {
        method: "GET",
        headers: { "content-type": "application/json" },
      }
    );
    const data = await response.json();
    setListItems(data);
    setIsLoading(false);
  };

  const GetAll = async () => {
    setListItems([]);
    const response = await fetch(
      `https://api-3sxs63jhua-uc.a.run.app/v1/todo/${user}`,
      {
        method: "GET",
        headers: { "content-type": "application/json" },
      }
    );
    const data = await response.json();
    setListItems(data);
    setIsLoading(false);
  };

  const selectFunction = (value: string) => {
    setIsLoading(true);
    if (value === "Incompletos") {
      GetIncompleted();
    } else if (value === "Completos") {
      GetCompleted();
    } else if (value === "Eliminar Todos"){
      ResetAll()      
    }else {
      GetAll();
    }

  };

  useEffect(() => {
    console.log(listItems);
  }, [listItems]);

  return (
    <IonPage>
      <Logo/>
      <IonContent className="backgroundToolbar">
        <IonLoading
          cssClass="my-custom-class"
          isOpen={isLoading}
          message={"Please wait..."}
        />
        <IonInput
          className="inputItem"
          value={item}
          placeholder="Escribi un item"
          onIonChange={(e) => setItem(e.detail.value!)}
          clearInput
        ></IonInput>
        <IonCard className="cardItems">
          <IonCardHeader className="headerContainer">
            <div className="headerTitle">To do list</div>
            <IonSelect multiple={true}
              interface="action-sheet"
              placeholder="Todos"
              onIonChange={(e) => selectFunction(e.target.value)}
            >
              <IonSelectOption value="Incompletos">Incompletos</IonSelectOption>
              <IonSelectOption value="Completos">Completos</IonSelectOption>
              <IonSelectOption value="Todos">Todos</IonSelectOption>
              <IonSelectOption value="Eliminar Todos">Eliminar Todos</IonSelectOption>
            </IonSelect>
          </IonCardHeader>
          <IonCardContent>
            {listItems.length > 0 && (
              <IonList>
                {listItems.map((item, index) => (
                  <Checkbox
                    Tarea={item}
                    key={item.message}
                    user={user!}
                    listItems={listItems}
                    setListItems={setListItems}
                  />
                ))}
              </IonList>
            )}
          </IonCardContent>
        </IonCard>
      </IonContent>
      <IonToolbar className="backgroundToolbar">
        <IonButton expand="block" className="btnStyle" onClick={AddItem}>
          Aceptar
        </IonButton>
      </IonToolbar>
    </IonPage>
  );
};

export default Home;